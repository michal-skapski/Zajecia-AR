using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlaceTrackedImages : MonoBehaviour
{
    private ARTrackedImageManager _trackedImageManager = null;

    public List<GameObject> arPrefabs;

    private readonly Dictionary<string, GameObject> _instatiatedPrefabs = new Dictionary<string, GameObject>();

    private void Awake()
    {
        _trackedImageManager = GetComponent<ARTrackedImageManager>();
    }

    private void OnEnable()
    {
        _trackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;

    }
    private void OnDisable()
    {
        _trackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }
    private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        // added
        foreach (var trackedImage in eventArgs.added)
        {
            var imageName = trackedImage.referenceImage.name;

            foreach (var curPrefab in arPrefabs)
            {
                if(string.Compare(curPrefab.name, imageName, System.StringComparison.OrdinalIgnoreCase) == 0 && !_instatiatedPrefabs.ContainsKey(imageName))
                {
                    var newPrefab = Instantiate(curPrefab, trackedImage.transform);
                    _instatiatedPrefabs[imageName] = newPrefab;
                }
            }
        }

        // update

        foreach(var trackedImage in eventArgs.updated)
        {
            _instatiatedPrefabs[trackedImage.referenceImage.name].SetActive(trackedImage.trackingState == TrackingState.Tracking); // it will easily change between true and false
        }

        // delete

        foreach (var trackedImage in eventArgs.removed)
        {
            Destroy(_instatiatedPrefabs[trackedImage.referenceImage.name]);
            _instatiatedPrefabs.Remove(trackedImage.referenceImage.name);

        }
    }
}